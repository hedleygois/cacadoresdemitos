package br.com.m2m.infotrek.cacadoresdemitos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ValidadorCartaoEnRouteTest {
	
	private Validador validador = null;
	
	@Before
	public void setUp(){
		validador = new ValidadorCartaoEnRoute();
	}
	
	@Test public void
	deveIdentificarCartaoEnRoute(){
		assertTrue(validador.valido("201400000000009"));
	}
	
	@Test public void
	naoDeveIdentificarCartaoEnRoute() {
		assertFalse(validador.valido("301400000000009"));
	}
	
	@Test public void
	deveIdentificarCartaoEnRouteComDigitosIniciais2149(){
		assertTrue(validador.valido("214925980592653"));
	}
	
	@Test public void
	daodeveIdentificarCartaoEnRouteComDigitosIniciais2149ETamanhoInpativel(){
		assertFalse(validador.valido("21490000000009"));
	}
	
}
