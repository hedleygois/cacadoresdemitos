package br.com.m2m.infotrek.cacadoresdemitos;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import br.com.m2m.infotrek.cacadoresdemitos.util.CartaoUtil;

public class CartaoUtilTest {

	private CartaoUtil util;
	
	@Before 
	public void setUp(){
		util = new CartaoUtil();
	}
	
	@Test public void
	deveAceitarUmCartaoValido(){
		assertTrue(util.LUHMMod10("4111111111111111"));
	}
	
	@Test public void
	naoDeveAceitarUmCartaoInvalido(){
		assertFalse(util.LUHMMod10("4111111111111112"));
	}
	
	@Test public void
	naoDeveAceitarUmCartaoInvalidoComLetras(){
		assertFalse(util.LUHMMod10("411111111111111as"));
	}
	
}
