package br.com.m2m.infotrek.cacadoresdemitos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ValidorCartaoMastercardTest {
	
	private Validador validador;
	
	@Before
	public void setUp(){
		validador = new ValidadorCartaoMastercard();
	}
	
	@Test public void
	deveIdentificarCartaoMastercard(){
		assertTrue(validador.valido("5555555555554444"));
	}
	
	@Test public void
	naoDeveIdentificarCartaoMastercard() {
		assertFalse(validador.valido("4555555555554444"));
	}
	
	@Test public void
	deveIdentificarCartaoMastercardComDigitosIniciais51() {
		assertTrue(validador.valido("5138495125550554"));
	}
	
	@Test public void
	deveIdentificarCartaoMastercardComDigitosIniciais52() {
		assertTrue(validador.valido("5274576394259961"));
	}
	
	@Test public void
	deveIdentificarCartaoMastercardComDigitosIniciais53() {
		assertTrue(validador.valido("5301745529138831"));
	}
	
	@Test public void
	deveIdentificarCartaoMastercardComDigitosIniciais54() {
		assertTrue(validador.valido("5404000000000001"));
	}
	
	@Test public void
	naoDeveIdentificarCartaoMastercardComDigitosIniciais56() {
		assertFalse(validador.valido("5655555555554444"));
	}
	
	@Test public void
	naoDeveIdentificarCartaoMastercardComDigitosIniciais50() {
		assertFalse(validador.valido("5655555555554444"));
	}
	
	@Test public void
	naoDeveValidarCartaoComQuantidadeDeDigitosIncompativeis(){
		assertFalse(validador.valido("540400000000000"));
	}

}
