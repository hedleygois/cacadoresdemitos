package br.com.m2m.infotrek.cacadoresdemitos;

public interface Validador {
	boolean valido(String numero);
}
