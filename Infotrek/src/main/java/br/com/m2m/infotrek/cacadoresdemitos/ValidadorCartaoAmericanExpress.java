package br.com.m2m.infotrek.cacadoresdemitos;

import br.com.m2m.infotrek.cacadoresdemitos.util.CartaoUtil;

public class ValidadorCartaoAmericanExpress implements Validador {

	@Override
	public boolean valido(String numero) {
		int cardID;
		if ((cardID = identificarBandeira(numero)) != -1)
			return new CartaoUtil().LUHMMod10(numero);
		return false;
	}

	private int identificarBandeira(String numero) {
		int codigo = CartaoUtil.INVALIDO;
		String digit2 = numero.substring(0, 2);
		
		/*
		 * ----* AMEX prefix=34 or 37* ---- length=15
		 */
		if (digit2.equals("34") || digit2.equals("37")) {
			if (numero.length() == 15){
				codigo = CartaoUtil.AMERICAN_EXPRESS;
			}
		}
		
		return codigo;
	}

}
