package br.com.m2m.infotrek.cacadoresdemitos;

import br.com.m2m.infotrek.cacadoresdemitos.util.CartaoUtil;

public class ValidadorCartaoMastercard implements Validador {

	@Override
	public boolean valido(String numero) {
		int cardID;
		if ((cardID = identificarBandeira(numero)) != -1)
			return new CartaoUtil().LUHMMod10(numero);
		return false;
	}

	private int identificarBandeira(String numero) {
		int codigo = CartaoUtil.INVALIDO;
		String digit2 = numero.substring(0, 2);

		/*
		 * ----------* MASTERCARD prefix= 51 ... 55* ---------- length= 16
		 */
		if (digit2.compareTo("51") >= 0 && digit2.compareTo("55") <= 0) {
			if (numero.length() == 16)
				codigo = CartaoUtil.MASTERCARD;
		}
		return codigo;
	}

}
