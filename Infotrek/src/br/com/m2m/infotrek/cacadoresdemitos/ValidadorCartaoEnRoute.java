package br.com.m2m.infotrek.cacadoresdemitos;

import br.com.m2m.infotrek.cacadoresdemitos.util.CartaoUtil;

public class ValidadorCartaoEnRoute implements Validador {

	@Override
	public boolean valido(String numero) {
		int CardID;
		if ((CardID = identificarBandeira(numero)) != -1)
			return new CartaoUtil().LUHMMod10(numero);
		return false;
	}

	private int identificarBandeira(String numero) {
		int codigo = CartaoUtil.INVALIDO;
		String digit4 = numero.substring(0, 4);
		
		/*
		 * -----* ENROU prefix=2014 or 2149* ----- length=15
		 */
		if (digit4.equals("2014") || digit4.equals("2149")) {
			if (numero.length() == 15)
				codigo = CartaoUtil.EN_ROUTE;
		}
		return codigo;
	}

}
